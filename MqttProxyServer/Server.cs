using System;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Client.Options;
using MQTTnet.Diagnostics;
using MQTTnet.Server;

namespace MqttProxyServer
{
    public class Server : IDisposable
    {
        private readonly Guid _id;
        private readonly ILogger _logger;
        private readonly IMqttNetLogger _mqttLogger;
        private readonly IMqttFactory _mqttFactory;
        private readonly IMqttServer _mqttServer;


        protected Server(ILogger logger)
        {
            _logger = logger;
            _id = Guid.NewGuid();
            _mqttLogger = new MqttNetLogger(_id.ToString());
            _mqttLogger.LogMessagePublished += MqttLogger_LogMessagePublished;

            _mqttFactory = new MqttFactory(_mqttLogger);
            _mqttServer = _mqttFactory.CreateMqttServer();
        }


        private void MqttLogger_LogMessagePublished(object sender, MqttNetLogMessagePublishedEventArgs e)
        {
            _logger.LogInformation("MqttLogger: {message}", e.LogMessage.Message);
        }


        public bool IsStarted => _mqttServer.IsStarted;


        public static async Task<Server> Run(ILogger logger)
        {
            var server = new Server(logger);

            var optionsBuilder = new MqttServerOptionsBuilder()
                .WithApplicationMessageInterceptor(async context =>
                {
                    if (context.ClientId != null)
                    {
                        await Task.Run(async () =>
                        {
                            await server.ProcessMessage(context);
                        });
                    }
                });

            await server._mqttServer.StartAsync(optionsBuilder.Build());

            return server;
        }

        protected virtual async Task ProcessMessage(MqttApplicationMessageInterceptorContext context)
        {
            var rawPayload = Encoding.UTF8.GetString(context.ApplicationMessage.Payload);
            
            try
            {
                ParseMessage(rawPayload, out ProxyHeader proxyHeader, out string proxyPayload);

                try
                {
                    await PublishPayload(context, proxyHeader, proxyPayload);

                    var debugPayload = new StringBuilder(
                        $"Payload published SUCCESSFULLY.{Environment.NewLine}" +
                        $"Message info:{Environment.NewLine}");
                    debugPayload.Append(MakeMessageDebugInfo(context));
                    debugPayload.Append($"Header info:{Environment.NewLine}");
                    debugPayload.Append(MakeHeaderDebugInfo(proxyHeader));
                    debugPayload.Append($"Payload:{Environment.NewLine}");
                    debugPayload.Append(proxyPayload);

                    await TryPublishDebug(context, debugPayload.ToString());

                    return;
                }
                catch (Exception ex)
                {
                    var debugPayload = new StringBuilder(
                        $"Payload publising FAILED.{Environment.NewLine}" +
                        $"Publishing exception: {ex.Message}{Environment.NewLine}" +
                        $"Message info:{Environment.NewLine}");
                    debugPayload.Append(MakeMessageDebugInfo(context));
                    debugPayload.Append($"Header info:{Environment.NewLine}");
                    debugPayload.Append(MakeHeaderDebugInfo(proxyHeader));
                    debugPayload.Append($"Payload:{Environment.NewLine}");
                    debugPayload.Append(proxyPayload);

                    await TryPublishDebug(context, debugPayload.ToString());

                    return;
                }
            }
            catch (Exception ex)
            {
                var debugPayload = new StringBuilder(
                    $"Payload parsing FAILED.{Environment.NewLine}" +
                    $"Parsing exception: {ex.Message}{Environment.NewLine}" +
                    $"Message info:{Environment.NewLine}");
                debugPayload.Append(MakeMessageDebugInfo(context));
                debugPayload.Append($"Raw payload:{Environment.NewLine}");
                debugPayload.Append(rawPayload);

                await TryPublishDebug(context, debugPayload.ToString());

                return;
            }
        }

        protected virtual void ParseMessage(string rawPayload, out ProxyHeader proxyHeader, out string proxyPayload)
        {
            try
            {
                var jsonDocument = JsonDocument.Parse(rawPayload);

                var proxyHeaderProperty = jsonDocument.RootElement.EnumerateObject().Where(p => p.Name == "proxyHeader").First();
                proxyHeader = JsonSerializer.Deserialize<ProxyHeader>(proxyHeaderProperty.Value.GetRawText());
                CheckAndNormalizeHeader(ref proxyHeader);

                var proxyPayloadProperty = jsonDocument.RootElement.EnumerateObject().Where(p => p.Name == "proxyPayload").First();
                proxyPayload = proxyPayloadProperty.Value.GetRawText();

                _logger.LogInformation("Payload parsed successfully.");
            }
            catch (Exception ex)
            {
                _logger.LogError("Message parsing exception: {exception}", ex.Message);
                
                throw;
            }
        }

        protected virtual void CheckAndNormalizeHeader(ref ProxyHeader header)
        {
            if (header.HostName == null)
            {
                throw new ArgumentNullException(nameof(header.HostName));
            }

            if (!header.Port.HasValue)
            {
                header.Port = (ushort?)(header.UseTls ? 8883 : 1883);
            }
        }

        protected virtual async Task PublishPayload(MqttApplicationMessageInterceptorContext context, ProxyHeader header, string payload)
        {
            try
            {
                using var mqttClient = _mqttFactory.CreateMqttClient();

                var optionsBuilder = new MqttClientOptionsBuilder()
                    .WithTcpServer(header.HostName, header.Port.Value)
                    .WithClientId(context.ClientId)
                    .WithCleanSession(header.UseCleanSession)
                    .WithCommunicationTimeout(TimeSpan.FromSeconds(header.ConnectionTimeout))
                    .WithKeepAlivePeriod(TimeSpan.FromSeconds(header.KeepAliveInterval));

                if (header.UseTls)
                {
                    optionsBuilder.WithTls();
                }

                if (header.UseCredentials)
                {
                    optionsBuilder.WithCredentials(header.UserName, header.Password);
                }

                await mqttClient.ConnectAsync(optionsBuilder.Build(), CancellationToken.None);

                var messageToPublish = new MqttApplicationMessageBuilder()
                    .WithTopic(context.ApplicationMessage.Topic)
                    .WithPayload(payload)
                    .WithQualityOfServiceLevel(context.ApplicationMessage.QualityOfServiceLevel)
                    .WithRetainFlag(context.ApplicationMessage.Retain)
                    .Build();

                await mqttClient.PublishAsync(messageToPublish, CancellationToken.None);

                _logger.LogInformation("Payload published successfully.");
            }
            catch (Exception ex)
            {
                _logger.LogError("Payload publish exception: {exception}", ex.Message);

                throw;
            }
        }

        protected virtual async Task TryPublishDebug(MqttApplicationMessageInterceptorContext context, string debugPayload)
        {
            try
            {
                var messageToPublish = new MqttApplicationMessageBuilder()
                    .WithTopic($"{context.ApplicationMessage.Topic}_debug")
                    .WithPayload(debugPayload)
                    .WithQualityOfServiceLevel(context.ApplicationMessage.QualityOfServiceLevel)
                    .WithRetainFlag(context.ApplicationMessage.Retain)
                    .Build();

                await _mqttServer.PublishAsync(messageToPublish, CancellationToken.None);

                _logger.LogInformation("Debug published successfully.");
            }
            catch (Exception ex)
            {
                _logger.LogError("Debug publish exception: {exception}", ex.Message);
            }
        }

        private string MakeMessageDebugInfo(MqttApplicationMessageInterceptorContext context)
        {
            return
                $"ClientId: {context.ClientId}{Environment.NewLine}" +
                $"Topic: {context.ApplicationMessage.Topic}{Environment.NewLine}" +
                $"Retain: {context.ApplicationMessage.Retain}{Environment.NewLine}" +
                $"QoS: {context.ApplicationMessage.QualityOfServiceLevel}{Environment.NewLine}";
        }

        private string MakeHeaderDebugInfo(ProxyHeader header)
        {
            return
                $"Server: {header.HostName}:{header.Port}{Environment.NewLine}" +
                $"UseTls: {header.UseTls}{Environment.NewLine}" +
                $"UseCleanSession: {header.UseCleanSession}{Environment.NewLine}" +
                $"ConnectionTimeout: {header.ConnectionTimeout}{Environment.NewLine}" +
                $"KeepAliveInterval: {header.KeepAliveInterval}{Environment.NewLine}" +
                $"UseCredentials: {header.UseCredentials}{Environment.NewLine}" +
                $"Credentials: {header.UserName}:{header.Password}{Environment.NewLine}";
        }

        
        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _mqttLogger.LogMessagePublished -= MqttLogger_LogMessagePublished;
                    _mqttServer?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Server()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    public sealed class ProxyHeader
    {
        [JsonPropertyName("hostName")]
        public string HostName { get; set; }

        [JsonPropertyName("port")]
        public ushort? Port { get; set; }

        [JsonPropertyName("useTls")]
        public bool UseTls { get; set; } = false;

        [JsonPropertyName("useCleanSession")]
        public bool UseCleanSession { get; set; } = true;

        [JsonPropertyName("connectionTimeout")]
        public ushort ConnectionTimeout { get; set; } = 30;

        [JsonPropertyName("keepAliveInterval")]
        public ushort KeepAliveInterval { get; set; } = 60;

        [JsonPropertyName("useCredentials")]
        public bool UseCredentials { get; set; } = false;

        [JsonPropertyName("userName")]
        public string UserName { get; set; } = "";

        [JsonPropertyName("password")]
        public string Password { get; set; } = "";
    }
}
